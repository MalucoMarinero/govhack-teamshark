'use strict'
fs = require 'fs'
path = require 'path'
mm = require 'minimatch'
wrench = require 'wrench'
coffee = require 'coffee-script'
{spawn} = require 'child_process'


lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet

mountFolder = (connect, dir) ->
  connect.static require('path').resolve(dir)





module.exports = (grunt) ->
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  grunt.initConfig {
    pkg: grunt.file.readJSON('package.json')
    compass:
      client:
        options:
          sassDir: 'src/client/sass'
          cssDir: 'src/client/css'
          environment: "development"
          config: 'config.rb'
    watch:
      client:
        files: [
          'src/client/**/*.{sass,scss}'
          'src/client/**/*.{html,css,js}'
        ]
        tasks: ['reloadDispatcher:client']
    loader:
      client:
        srcDir: 'src/client/js/app'
        dest: 'src/client/js/loader.js'
        priorityMatches: ['**/main.js', '**/module.js']
        ignorePattern: 'loader.js'
        prefix: 'js/app/'
        varName: 'govHackLoader'
    reloadDispatcher:
      client:
        "**/*.{sass,scss}" : ['compass:client']
        "**/*.js" : ['loader:client']
        "**/*.{html,css,js}" : ['livereload']
    connect:
      options:
        port: 8085
        hostname: 'localhost'
      client:
        options:
          middleware: (connect) ->
            return [
              lrSnippet
              mountFolder connect, 'src/client'
            ]
  }

  grunt.renameTask 'regarde', 'watch'

  grunt.registerTask 'client', ->
    grunt.task.run [
      'connect:client'
      'livereload-start'
      'watch:client'
    ]

  grunt.registerMultiTask 'loader', 'Create a script loader from a file list', ->
    options = @options()
    done = @async()

    @files.forEach (f) ->
      console.log "Compiling loader for #{ f.srcDir } to #{ f.dest }"
      dest = f.dest
      loaderCode = "window.#{ f.varName } = (cb) -> head.js "
      loaderPaths = []
      files = wrench.readdirSyncRecursive(f.srcDir).filter mm.filter "**/*.js"

      files.forEach (filePath) ->
        if not mm(filePath, f.ignorePattern)
          loaderPaths.push f.prefix + filePath

      rankPath = (path) ->
        i = 0
        for priorityMatch in f.priorityMatches
          if mm(path, priorityMatch)
            return i
          i += 1
        return f.priorityMatches.length

      loaderPaths.sort (a, b) ->
        if rankPath(a) < rankPath(b) then return -1
        if rankPath(a) > rankPath(b) then return 1
        return 0
      
      loaderPaths = loaderPaths.map (path) -> '"' + path + '"'
      loaderCode += loaderPaths.join ', '
      loaderCode += ', -> cb()'
      loaderJS = coffee.compile loaderCode, bare: true
      fs.writeFile dest, loaderJS, (err) ->
        if err then console.log err
        done()


  grunt.registerMultiTask 'reloadDispatcher', 'Run tasks based on extensions.', ->
    for pattern, tasks of this.data
      if grunt.regarde.changed.some mm.filter pattern
        grunt.task.run tasks


