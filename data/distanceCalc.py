# -*- coding: utf-8 -*-

australia_lat = -35.25
australia_lon = 149.0 + (8 / 60)

import math
import csv

def distance(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371 # km

    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d

def llstr_to_float(latstr):
    latstr, nesw = latstr.split("'")
    deg, minutes = latstr.split('°')
    if nesw in ['N', 'E']:
        return float(deg) + (float(minutes) / 60)
    else:
        return 0 - (float(deg) + (float(minutes) / 60))

with open('latlong.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
    with open('ausdistances.csv', 'w+') as distfile:
        for row in reader:
            country, capital, lat, lon = row
            lat = llstr_to_float(lat)
            lon = llstr_to_float(lon)
            dis = distance((lat, lon), (australia_lat, australia_lon))
            line = '"%s";"%s";%s\n' % (country, capital, dis)
            distfile.write(line)

