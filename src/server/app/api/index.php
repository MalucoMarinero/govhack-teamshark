<?php
	/**
	 * Commodity API for GovHack Tas
	 * -----------------------------
	 *
	 * Data loaded from the following sources:
	 * 		DSPL countries.csv (Google) - https://code.google.com/p/dspl/source/browse/datasets/google/canonical/countries.csv
	 *		Country and commodity trade data spreadsheet (data.gov.au) - http://data.gov.au/dataset/country-and-commodity-trade-data-spreadsheet-2/
	 *
	 * Note:
	 * 		Some Country data does not match between the two sources. These 30~ were manually corrected.
	 * 		Distance to australia is pre-calculated. Percentages are relative to total price for a commodity in THAT year
	 *
	 *
	 * Usage:
	 * 		API is very straight forward, predominantly for use of loading in shipping data of commodity items. JSONP is supported for cross script
	 *
	 * 		//url.site/commodity/11 - Where 11 is the commodity code
	 * 		//url.site/commodity/11/2009 - Where 2009 is the year 2009
	 *
	 * 		//url.site/commodity_search/word - Where word is a search phrase to find a commodity. Should only be used when locating commodity codes.
	 *
	 */

	require '../../init.php';

	// http://stackoverflow.com/questions/8719276/cors-with-php-headers
	cors();


	// Create instance of the app
    $app = new \Slim\Slim(array(
        'mode' => 'development',
        'debug' => true
    ));

	// Return information about a commodity
	$app->get('/commodity/:code(/:year)', function ($code, $year=2011) use ($app)  {

		// Do some callback stuff
		$callback = $app->request()->get('callback');
		$app->response()->header("Content-Type", "application/json");

		// Find the commodity name based on code
		$commodity_name = dibi::select("name")->from("commodities")->where("code=%s", $code)->fetchSingle();

		// Find the countries related to this commodity
		$commodities_country = dibi::query('
				select c.name, s.price, s.percentage, c.distance_to_aus as distance
				from commodities as i, countries as c, shippings as s
				where s.year=%s and i.code=%s and s.country_id=c.id and s.commodity_id=i.id and s.direction=0 and c.distance_to_aus is not null
				order by s.percentage DESC
				', $year, $code)->fetchAll();

		// Do some number addition!
		$commodity_total = 0;
		foreach($commodities_country as $row){
			$commodity_total += $row['price'];
		}

		// Build our array to push out to JSON
		$dataArray = array(
			'name' => $commodity_name,
			'total' => $commodity_total,
			'source' => $commodities_country
		);

		// Encode it like a boss
		$app->contentType('application/javascript');
		echo sprintf("%s(%s)", $callback, json_encode($dataArray));
	});

	// Find a commodity by a phrase and JSON it out. Don't use this for production
	$app->get('/commodity_search/:phrase', function ($phrase) use ($app)  {
		$commodities = dibi::query('select * from commodities where name LIKE %~like~', $phrase)->fetchAll();
		$app->contentType('application/javascript');
		json_encode($commodities);
	});

	$app->post("/results", function () use($app) {
		// Do some callback stuff
		$app->response()->header("Content-Type", "application/json");

		$result_data = $app->request()->post();

		dump($result_data);

		//print_r($result_data);

		//$result = $db->books->insert($book);

		//echo $result_data;
		//json_encode($result_data['totalFootprint']);
	});



	// Run the app!
    $app->run();