<?php
	/**
	 * Load the commodity data into the database.
	 * Pretty much as we are using DIBI, this could be any SQL Lang
	 */
	require '../init.php';

	$row = 1;
	if (($handle = fopen("Cty by commodity FY2012.csv", "r")) !== false) {
		while (($data = fgetcsv($handle, 0, ",")) !== false) {
			if($row != 1){
				$country_name = $data[2];
				$country_id = dibi::select("id")->from("countries")->where("name = %s", $country_name)->fetchSingle();

				if(!$country_id){
					$country_insert = array(
						'name' => $country_name,
					);
					dibi::insert("countries", $country_insert)->execute();
					$country_id = dibi::getInsertId();
				}

				$commodity_split = explode(" ", $data[3], 2);

				$commodity_id = dibi::select("id")->from("commodities")->where("code = %s", $commodity_split[0])->fetchSingle();

				if(!$commodity_id){
					$commodity_insert = array(
						'name' => $commodity_split[1],
						'code' => $commodity_split[0]
					);
					dibi::insert("commodities", $commodity_insert)->execute();
					$commodity_id = dibi::getInsertId();
				}
				$shipping_import = array(
					'commodity_id' => $commodity_id,
					'country_id' => $country_id,
					'price' => $data[4]."000",
					'year' => substr($data[1], -4),
					'direction' => ($data[0] == "Imports") ? 0 : 1
				);
				dibi::insert("shippings", $shipping_import)->execute();
				echo "Row #".$row."\r\n";
			}
			$row++;
		}
		fclose($handle);
	}
