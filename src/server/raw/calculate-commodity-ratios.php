<?php
	/**
	 * Calculate the commodity ratios for the frontend
	 */
	require '../init.php';

	foreach (dibi::select("code")->from("commodities")->fetchAll() as $commodity) {

		$commodities_country = dibi::query('
			select s.id, i.name as item_name, c.name as country_name, s.price
			from commodities as i, countries as c, shippings as s
			where s.year=200 and i.code=%s and s.country_id=c.id and s.commodity_id=i.id and s.direction=0
			', $commodity['code']);

		$commodities_country = $commodities_country->fetchAll();

		// Calculate total price for the commodity
		$total_price = 0;
		foreach ($commodities_country as $row) {
			$total_price += $row['price'];
		}

		// Update the shipping info with the percentage
		foreach ($commodities_country as $row) {
			dibi::update("shippings", array("percentage" => round(($row['price'] / $total_price),4)))->where("id = %i", $row['id'])->execute();
		}

	}
