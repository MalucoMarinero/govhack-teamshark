<?php
	/**
	 * Load the country information into the database.
	 * Pretty much as we are using DIBI, this could be any SQL Lang
	 */
	require '../init.php';

	// Australian information
	$aus_lat = -25.2744;
	$aus_long = 133.775;
	$aus_location = new \Lootils\Geo\Location($aus_lat, $aus_long);  // The numbers as dec format floats.

	$row = 1;
	if (($handle = fopen("countries.csv", "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
			if($row != 1){

				$country_id = dibi::select("id")->from("countries")->where("name = %s", $data[3])->fetchSingle();
				$location = new \Lootils\Geo\Location($data[1], $data[2]);

				if($country_id){
					$array = array(
						"code" => $data[0],
						"latitude" => $data[1],
						"longitude" => $data[2],
						"distance_to_aus" => ($location->distance($aus_location)/1000)
					);

					dibi::update("countries", $array)->where("id = %i", $country_id)->execute();
				}

				echo "\r\nRow #".$row."\r\n";
			}
			$row++;
		}
		fclose($handle);
	}
