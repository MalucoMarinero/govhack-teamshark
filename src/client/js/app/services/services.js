(function() {



teamSharkApp.service('userChoices', function($filter, $rootScope){
  var choiceService = {
    totalFootprint: 0,
    AVERAGE_BASELINE: 12900,
		CAP: 26425,
    summaryItem: null,
    previewItem: null,
    openMousePreview: function(item){
      choiceService.previewItem = item;
      $rootScope.$broadcast('updatePreviewItem');
    },
    closeMousePreview: function(item){
      choiceService.previewItem = null;
      $rootScope.$broadcast('updatePreviewItem');
    },
    showSummary: function(item){
      choiceService.summaryItem = item;
      $rootScope.$broadcast('updateSummaryItem');
    },
    clearSummary: function(item){
      choiceService.summaryItem = null;
      $rootScope.$broadcast('updateSummaryItem');
    },
    selectedItems: [],
    removeItem: function(item){
      var index = choiceService.selectedItems.indexOf(item);
      if (index !== -1){
        choiceService.selectedItems.splice(index, 1);
      }
    },
    addItem: function(item){
      if (choiceService.selectedItems.indexOf(item) === -1){
        choiceService.selectedItems.push(item);
        choiceService.totalFootprint = choiceService.selectedItems.reduce(
          function(acc, item){
            return acc + $filter('getYearlyCarbon')(item)
          }, 0
        );
      }
    }
  };
  return choiceService;
});



teamSharkApp.service('dataService', function($http, $rootScope, $filter){

  var dataObj = {
    availableItems: []
  };

  $http.get('/data/dummyData.json').success(function(data){
    data.forEach(function(item){
      dataObj.availableItems.push(item);
      if (item.sitc) {
        $http.jsonp('http://govhack2013.shaun.pw/api/commodity/' + item.sitc + '?callback=JSON_CALLBACK')
            .success(function(sourceList){
          if (sourceList.source.length > 0) {
            fetchedSources = []
            sourceList.source.forEach(function(source){
              if (source.distance) {
                fetchedSources.push({
                  country: source.name, distanceKm: source.distance,
                  ratio: source.percentage
                });
              }
            });
            item.costs.shipping.sources = fetchedSources;
          }
        }).error(function(error){});
      }
    });
  });

  return dataObj;

});

}).call(this);
