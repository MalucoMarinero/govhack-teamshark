(function() {

teamSharkApp.directive('footprintItem', function($filter, userChoices){
  var currentLength = 0,
      currentColorScheme = [];
  
  var directiveDefinition = {
    restrict: 'AEC',
    link: function($scope, elem, attrs) {
      var parentScope = (elem.hasClass('userItem')) ?
                        $scope.$parent : $scope;

      var updateHeight = function(){
      	if (currentLength !== parentScope.selectedItems.length) {
      		currentLength = parentScope.selectedItems.length + 1;
      		currentColorScheme = $.xcolor.monochromatic('#3da843', parentScope.selectedItems.length + 3);
      	}
        if ($scope.scale === 'fullScale') {
          if (elem.hasClass('baseFootprint')) {
            elem.css('background', currentColorScheme[0]);
          } else if (elem.hasClass('averageFootprintRemainder')) {
            elem.css('background', '#000');
          } else {
            var color = $.xcolor.lighten(currentColorScheme[parentScope.selectedItems.indexOf($scope.item) + 1]);
            elem.css('background', color);
          }
        } else {
          if (elem.hasClass('averageFootprintRemainder')) {
            elem.css('background', '#3da843');
          } else {
            elem.css('background', '#000000');

          }
        }

        if ($scope.scale === 'fullScale') {
          if (elem.hasClass('baseFootprint') || elem.hasClass('averageFootprintRemainder')) {
            elem.height(0);
          } else {
            elem.height(
              elem.parent('ul').height() / parentScope.selectedItems.length);
          }
        } else {
          var ratio;
          if (elem.hasClass('averageFootprintRemainder')) {
            ratio = (userChoices.CAP - userChoices.AVERAGE_BASELINE - userChoices.totalFootprint) / userChoices.CAP;
          } else if (elem.hasClass('baseFootprint')) {
            var checkRatio = (userChoices.CAP - userChoices.AVERAGE_BASELINE - userChoices.totalFootprint) / userChoices.CAP;
            ratio = userChoices.AVERAGE_BASELINE / userChoices.CAP;
            if (checkRatio < 0.2) {
              $(elem).find('.label').css('top', 400);
            }
          } else {
            ratio = $filter('getYearlyCarbon')($scope.item) / userChoices.CAP;
          }
          elem.height(parseInt(elem.parent('ul').height() * ratio));
        }
      };

      parentScope.$watch('selectedItems.length', function(){
        updateHeight();
      });
      parentScope.$watch('$location.path()', function(){
        updateHeight();
      });
    }
  };

  return directiveDefinition;
});

}).call(this);
