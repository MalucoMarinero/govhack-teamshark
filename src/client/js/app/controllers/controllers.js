(function() {

teamSharkApp.controller('selectionCtrl', function($scope, $http, dataService, userChoices){
  $scope.availableItems = dataService.availableItems;
  $scope.selectedItem = null;
  $scope.selectItem = function(item) {
    $scope.selectedItem = item;
    maskHeight();
  };
  $scope.unselectItem = function() {
    $scope.selectedItem = null;
  };

  $scope.selectedItems = userChoices.selectedItems;

  $scope.checkSelected = function(item) {
    return $scope.selectedItems.indexOf(item) !== -1;
  }

  $scope.$on('updatePreviewItem', function(){
    $scope.selectedItem = userChoices.previewItem;
    console.log('updatePreviewItem', $scope.selectedItem);
  });

  $scope.addItem = function(item){
    userChoices.addItem(item);
    $scope.selectedItem = null;
  };
  $scope.removeItem = function(item){
    userChoices.removeItem(item);
    $scope.selectedItem = null;
  };
});


teamSharkApp.controller('resultsCtrl', function($scope, $http, userChoices, $location){
  userChoices.summaryItem = null;
  $scope.baseline = userChoices.AVERAGE_BASELINE;
  $scope.currentTotal = userChoices.totalFootprint;
  $scope.summaryItem = userChoices.summaryItem;
  $scope.selectedItems = userChoices.selectedItems;
  $scope.$on('updateSummaryItem', function(){
    $scope.summaryItem = userChoices.summaryItem;
    setTimeout(maskHeight, 500);
  });
  $scope.loadGeneralBaseline = function() {
    switch ($location.path()) {
      case "/results":
        userChoices.clearSummary(); break;
    }
  };
  $scope.loadItemSummary = function(item) {
    switch ($location.path()) {
      case "/results":
        userChoices.showSummary(item); break;
    }
  };
});


teamSharkApp.controller('footprintCtrl', function($scope, $http, userChoices, $location, $filter){
  $scope.selectedItems = userChoices.selectedItems;
  $scope.$location = $location;
  $scope.$watch('$location.path()', function(newPath){
    switch (newPath) {
      case "/results":
        $scope.scale = "proportional"; break;
      case "/summary":
        $scope.scale = "proportional"; break;
      case "/summary2":
        $scope.scale = "proportional"; break;
      default:
        $scope.scale = "fullScale"; break;
    }
  });
  $scope.mouseLeavePreview = function() {
    userChoices.closeMousePreview();
  };
  $scope.mouseEnterPreview = function(item) {
  	maskHeight();
    userChoices.openMousePreview(item);
  };
  $scope.mouseCatch = function(item, event) {
    console.log('mouseCatch', item, event);
  };

  $scope.loadItemSummary = function(item) {
  	
    switch ($location.path()) {
      case "/results":
        userChoices.showSummary(item); break;
    }
  };
  $scope.loadGeneralBaseline = function() {
    switch ($location.path()) {
      case "/results":
        userChoices.clearSummary(); break;
    }
  };

});

teamSharkApp.controller('summaryCtrl', function($scope, $http, userChoices){

		//bad hard coding
		//userChoices.totalFootprint = 60770.0736;

		var users_calculated_co2_ton = (userChoices.totalFootprint + userChoices.AVERAGE_BASELINE) / 1000;
		var average_individual_co2_ton = 26.425;

		var average_estimated_no_carbon_price = 23.09;
		var average_estimated_carbon_price = 21.247;
		var average_estimated_co2_5 = 17.907;
		var average_estimated_co2_15 = 16.02;
		var average_estimated_co2_25 = 14.13;


		$scope.users_calculated_co2_ton = users_calculated_co2_ton; //metric ton
		$scope.average_individual_co2_ton = average_individual_co2_ton;

		if(average_individual_co2_ton < users_calculated_co2_ton) {
				$scope.evaluation = "bad";
    } else if (average_estimated_co2_5 > users_calculated_co2_ton) {
				$scope.evaluation = "good";
    } else {
				$scope.evaluation = "medium";
    }

});



		teamSharkApp.controller('progressCtrl', function($scope, $http, $location){
  $scope.$location = $location;
});
                       


}).call(this);
