(function() {
  window.teamSharkApp = angular.module('teamSharkApp', []);
  teamSharkApp.config(function($routeProvider) {
    $routeProvider
      .when('/', {templateUrl: "partials/select.html",
                  controller: "selectionCtrl"})
      .when('/results', {templateUrl: "partials/results.html",
                         controller: "resultsCtrl"})
		  .when('/summary', {templateUrl: "partials/summary-current.html",
				                 controller: "summaryCtrl"})
		  .when('/summary2', {templateUrl: "partials/summary-future.html",
				                 controller: "summaryCtrl"})
		    .otherwise({redirectTo: '/'});
                     
  });
}).call(this);


$(document).ready(function() {	
	$.fancybox(
		{
			padding : 30,
			href : './img/splash.jpg',
			closeBtn : '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			overlay : {
				speedIn : 500,
				speedOut : 500
			}
		}
	);
	
	$('.product ').on('click', function() {
		manualHeights();
	});
	
});

$(window).load(function() {
	manualHeights();
	maskHeight();
	moveMask();
});

$(window).resize(function() {
	manualHeights();
	maskHeight();
	moveMask();	
});


function manualHeights(){
	var windowHeight = $(window).height();
	var documentHeight = $(document).height();
	
	if($('.footprint').height() < windowHeight ){
		var footprintHeight = windowHeight + 20;
		$('.footprint').css('height', footprintHeight);
	}
		
	var footprintHeight = $('.mask').height();
	var footprintItems = $('.footprint ul');
	footprintItems.css('height', footprintHeight - 22);
}

function maskHeight(){
	var windowHeight = $(window).height();
	var overlayOffset = 40 + $('.logo img').height()
	
	windowHeight = windowHeight - overlayOffset;
		
	$('.overlay').css({
		'height': windowHeight,
		'top': overlayOffset
	});
}

function moveMask(){
	var mask = jQuery('.mask');
	var maskWidth = mask.width();
	var footprintWidth = jQuery('.footprint').width();
	var left = -(maskWidth - footprintWidth) / 2 + 'px';
	mask.css('left', left);
}
