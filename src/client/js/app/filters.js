(function() {

var AIR_FREIGHT_CO2 = 0.0007,
    ROAD_FREIGHT_CO2 = 0.00018,
    OCEAN_FREIGHT_CO2 = 0.000011,
    RAIL_FREIGHT_CO2 = 0.00002,
    KWH_TO_CO2 = 0.8;

teamSharkApp.filter('shippingToCarbon', function(){
  return function(shippingData){
    if (!shippingData) { return '' }
    var kgDistRatios = [];
    shippingData.sources.forEach(function(shippingSource){
      kgDistRatios.push({
        country: shippingSource.country,
        distanceKm: shippingSource.distanceKm,
        kgPerItem: shippingData.kgPerItem * shippingSource.ratio
      });
    });

    var kgKm = 0;
    kgDistRatios.forEach(function(ratio){
      kgKm += (ratio.distanceKm * ratio.kgPerItem)
    });

    var co2 = 0;
    shippingData.methods.forEach(function(shipMethod){
      switch (shipMethod.type) {
        case "ocean":
          co2 += (kgKm * shipMethod.ratio * OCEAN_FREIGHT_CO2); break;
        case "air":
          co2 += (kgKm * shipMethod.ratio * AIR_FREIGHT_CO2); break;
        case "rail":
          co2 += (kgKm * shipMethod.ratio * RAIL_FREIGHT_CO2); break;
        case "road":
          co2 += (kgKm * shipMethod.ratio * ROAD_FREIGHT_CO2); break;
      }
    });
    return co2;
  }
});

teamSharkApp.filter('kWhToCarbon', function($filter){
  return function(kWhPerYear) {
    if (!kWhPerYear && kWhPerYear !== 0) { return '' }
    return kWhPerYear * KWH_TO_CO2;
  }
});

teamSharkApp.filter('nameToSlug', function($filter){
  return function(name) {
	  if(!name){
	  	return null;
	  }
    return name.split(' ').join('').replace('\'','').toLowerCase();
    
  }
});

teamSharkApp.filter('getYearlyCarbon', function($filter){
  return function(item) {
    if (!item) { return '' }
    var yearlyCarbon = 0;

    yearlyCarbon += (item.amountPerYear *
                     item.costs.production.kgCarbonPerItem);

    yearlyCarbon += (item.amountPerYear *
                     $filter('shippingToCarbon')(item.costs.shipping));

    yearlyCarbon += (item.costs.usage.kWhPerYear * KWH_TO_CO2);

    return yearlyCarbon;
  }
});



}).call(this);
