```code
      ,|
     / ;
    /  \
   : ,'(
   |( `.\
   : \  `\       \.        __,_____( (
    \ `.         | `.     / __.==--"-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
     \  `-._     ;   \   /#(-'     ( (
      \     ``-.'.. _ `._`-'
       `. `-.            ```-...__
        .'`.        --..          ``-..____
      ,'.-'`,_-._            ((((   <o.   ,'
           `' `-.)``-._-...__````  ____.-'
               ,'    _,'.--,---------'
           _.-' _..-'   ),'
          ``--''        `
  ______                        __                                __               __  
 /_  __/__  ____ _____ ___     / /   ____ _____  ____  __________/ /_  ____ ______/ /__
  / / / _ \/ __ `/ __ `__ \   / /   / __ `/_  / / __ \/ ___/ ___/ __ \/ __ `/ ___/ //_/
 / / /  __/ /_/ / / / / / /  / /___/ /_/ / / /_/ /_/ / /  (__  ) / / / /_/ / /  / ,<   
/_/  \___/\__,_/_/ /_/ /_/  /_____/\__,_/ /___/\____/_/  /____/_/ /_/\__,_/_/  /_/|_|  
                                                                                       
```

Project name: ImprintMe
-
Your Carbon Imprint.

How do your daily consumables and purchases affect your carbon footprint, and how does it compare to the Australian 2020 targets?

Find out with ImpressMe, our Carbon Footprint calculator based on Country and Commodity Trade Data from data.gov.au 


Team members:
- Zakk Goodsell
- Shaun Walker
- Marcus Bendall
- James Rakich
- Jonny Scholes
- Benjamin Lambert 

Data sources:
- DSPL countries.csv (Google) https://code.google.com/p/dspl/source/browse/datasets/google/canonical/countries.csv
- Country and commodity trade data spreadsheet (data.gov.au) http://data.gov.au/dataset/country-and-commodity-trade-data-spreadsheet-2/



Client Install:
```code
in base folder
sudo npm install -g grunt-cli
sudo npm install

grunt client
```


Server Install:

```code
in /src/server
curl -sS https://getcomposer.org/installer | php
php composer.phar install

Load in the dump.sql, edit init.php
Run the php code on a server somewhere
```

The MIT License (MIT)

Copyright (c) 2013 Team Lazorshark

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
